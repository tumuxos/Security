# TumuxOS security repository

Repository to store a copy of the public keys, some signature files, warrant canaries, etc...

## TumuxOS Master and Backup Key

The master key fingerprint:
```bash
pub   dsa3072 2022-02-11 [SC]
      E66C 6215 074B 6FDF EBB2  6E44 C55E F096 A89C 8D08
uid          [  ultime ] TumuxOS Master Key
```

The backup key fingerprint:
```bash
pub   dsa3072 2022-02-11 [SC]
      BCE5 C7F9 56B2 44B3 940F  49E9 68F9 0205 1F4B 9D3A
uid          [  ultime ] TumuxOS Backup Master Key
```

**/!\ note that the backup keep should NEVER be used for anything that replacing the master key**

## Verifying a signatures

Please read [this document](https://gitlab.com/tumuxos/Security/-/blob/main/VERIFYING%20SIGNATURES.md) to see how you can trust our files

## Check our canaries

You can find all of our canaries [here](https://gitlab.com/tumuxos/Security/-/tree/main/canaries) with they signatures file.
Please don't forget to validate each canary with his detached signature. At least the current admin or dedicated one must sign.
Other member are invited to sign each canary-to-be if they still trust the project.

Each canary must contain the following chapter

#### Statements
* The date of the canary
* The TumuxOS Master Key fingerprint
* The main statement that no warrants where served to us
* The next canary release

#### Special annoucements (optional)
Any informations that we want to provide you within the canary. It may contain any declarations about an incoming canary template update at least **one year in advance** (2 canaries for now).

#### Disclaimers and notes
A fixed text that should remind the end user about the concept of the canary watcher.

#### Proof of freshness
* `date --utc --iso-8601=ns` set **after** every other proof (to help find and verify)
* Top 5 headline from the BBC news
* METAR/NOTAM data on an airport (any airport)
* The index and its random data from the League of Entropy
* The latest block hash (bitcoin) at that moment

#### Footnote (optional)
Any footnote from the rest of the documents

**no other sections are permited**

The canary must start with `---===[ TumuxOS Canary XXX ]===---` where XXX is the current canary number.

