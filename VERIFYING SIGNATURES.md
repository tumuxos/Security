Verifying signatures
====================

The TumuxOS Project uses [digital signatures](https://en.wikipedia.org/wiki/Digital_signature) to guarantee the authenticity and integrity of certain important assets like the kernel binary, every commits and tags on git, important news, etc.

This page explains how to verify those signatures. It is extremely important for your security to understand and apply these practices.

What digital signatures can and cannot prove
--------------------------------------------

Most people — even programmers — are confused about the basic concepts underlying digital signatures. Therefore, most people should read this section, even if it looks trivial at first sight.

Digital signatures can prove both **authenticity** and **integrity** to a reasonable degree of certainty. **Authenticity** ensures that a given file was indeed created by the person who signed it (i.e., that a third party did not forge it). **Integrity** ensures that the contents of the file have not been tampered with (i.e., that a third party has not undetectably altered its contents _en route_).

Digital signatures **cannot** prove, e.g., that the signed file is not malicious. In fact, there is nothing that could stop someone from signing a malicious program (and it happens from time to time in reality).

The point is that we must decide who we will trust (e.g., Linus Torvalds, Microsoft, a friend) and assume that if a trusted party signed a given file, then it should not be malicious or negligently buggy. The decision of whether to trust any given party is beyond the scope of digital signatures. It’s more of a social and political decision.

By verifying all the files we download that purport to be authored by a party we’ve chosen to trust, we eliminate concerns since we can easily detect whether any files have been tampered with (and subsequently choose to refrain from executing, installing, or opening them).

However, for digital signatures to make sense, we must ensure that the public keys we use for signature verification are the original ones. Anybody can generate a cryptographic key that purports to belong to any given project, but of course only the keys that the real developers generate are the genuine ones. The rest of this page explains how to verify the authenticity of the various keys used in the project and how to use those keys to verify certain important assets.

OpenPGP software
----------------

We use [PGP](https://en.wikipedia.org/wiki/Pretty_Good_Privacy) (specifically, the [OpenPGP](https://en.wikipedia.org/wiki/Pretty_Good_Privacy#OpenPGP) standard). Before we begin, you’ll need software that can manage PGP keys and verify PGP signatures. Any program that complies with the OpenPGP standard will do, but here are some examples for popular operating systems:

**Linux:** [GnuPG](https://gnupg.org/download/index.html) ([documentation](https://www.gnupg.org/documentation/)). Open a terminal and use the `gpg2` command. If you don’t already have GnuPG installed, install it via your distro’s package manager or from the GnuPG website.

**Mac:** [GPG Suite](https://gpgtools.org/) ([documentation](https://gpgtools.tenderapp.com/kb)). Open a terminal to enter commands.

**Windows:** [Gpg4win](https://gpg4win.org/download.html) ([documentation](https://www.gpg4win.org/documentation.html)). Use the Windows command line (`cmd.exe`) to enter commands.

Throughout this page, we’ll use GnuPG via the `gpg2` command. If that doesn’t work for you, try `gpg` instead. If that still doesn’t work, please consult the documentation for your specific program (see links above) and the [troubleshooting FAQ](#troubleshooting-faq) below.

How to import and authenticate the TumuxOS Master Signing Key
-------------------------------------------------------------

Every important assets of the project are digitally signed by an official team member’s key or by a release signing key (RSK). Each such key is, in turn, signed by the [**TumuxOS Master Signing Key (TMSK)**](https://tumuxos.gitlab.io/keys/MasterKey.pub) (`0xE66C6215074B6FDFEBB26E44C55EF096A89C8D08`). In this way, the TMSK is the ultimate root of trust for TumuxOS.

Before we proceed, you must first complete the prerequisite step of [installing OpenPGP software](#openpgp-software).

Now, there are several ways to get the TMSK.
    
*   Fetch it with GPG:
```bash
        $ gpg2 --fetch-keys https://tumuxos.gitlab.io/keys/MasterKey.pub
```
    
*   Get it from a public [keyserver](https://en.wikipedia.org/wiki/Key_server_%28cryptographic%29#Keyserver_examples) (specified on first use with `--keyserver <URI>` along with keyserver options to include key signatures), e.g.:
```bash
        $ gpg2 --keyserver-options no-self-sigs-only,no-import-clean --keyserver hkp://keyserver.ubuntu.com --recv-keys 0xE66C6215074B6FDFEBB26E44C55EF096A89C8D08
```
    
*   Download it as a file, then import the file.
    
    Here are some example download locations:
    
    *   [TumuxOS Security repository](https://gitlab.com/tumuxos/Security/-/raw/main/keys/MasterKey.pub)
    *   [TumuxOS website](https://tumuxos.gitlab.io/keys/MasterKey.pub)
    
    Once you have the key as a file, import it:
```bash
        $ gpg2 --import /<PATH_TO_FILE>/MasterKey.pub
```

Once you’ve obtained the TMSK, you must verify that it’s authentic rather than a forgery. Anyone can create a PGP key with the name “TumuxOS Master Key” and the short key ID `0xA89C8D08`, so you cannot rely on these alone. You also should not rely on any single website, not even over HTTPS.

So, what _should_ you do? One option is to use the PGP [Web of Trust](https://en.wikipedia.org/wiki/Web_of_trust).

Perhaps the most common route is to rely on the key’s fingerprint. Every PGP key has a fingerprint that uniquely identifies it among all PGP keys (viewable with `gpg2 --fingerprint <KEY_ID>`). Therefore, if you know the genuine TMSK fingerprint, then you always have an easy way to confirm whether any purported copy of it is authentic, simply by comparing the fingerprints.

For example, here is the TMSK fingerprint:
```bash
    pub   dsa3072 2022-02-11
          Key fingerprint = E66C 6215 074B 6FDF EBB2  6E44 C55E F096 A89C 8D08
    uid TumuxOS Master Key
```

But how do you know that this is the real fingerprint? After all, [this website could be compromised](/faq/#should-i-trust-this-website), so the fingerprint you see here may not be genuine. That’s why we strongly suggest obtaining the fingerprint from _multiple independent sources in several different ways_.

Here are some ideas for how to do that:

*   Check the fingerprint on various websites.
*   Check against PDFs, photographs, and videos in which the fingerprint appears.
*   Download old archive from different sources and check the included TMSK.
*   Ask people to post the fingerprint on various mailing lists, forums, and chat rooms.
*   Repeat the above over Tor.
*   Repeat the above over various VPNs and proxy servers.
*   Repeat the above on different networks (work, school, internet cafe, etc.).
*   Text, email, call, video chat, snail mail, or meet up with people you know to confirm the fingerprint.
*   Repeat the above from different computers and devices.

Now that you’ve imported the authentic TMSK, set its trust level to “ultimate” so that it can be used to automatically verify all the keys signed by the TMSK.
```bash
    $ gpg2 --edit-key 0xE66C6215074B6FDFEBB26E44C55EF096A89C8D08
    gpg (GnuPG) 1.4.18; Copyright (C) 2014 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.
    
    pub  dsa3072 created: 2022-02-11  expires: never       usage: SC
                         trust: unknown       validity: unknown
    [ unknown] (1). TumuxOS Master Key
    
    gpg> fpr
    pub   dsa3072 created: 2022-02-11 TumuxOS Master Key
    Primary key fingerprint: E66C 6215 074B 6FDF EBB2  6E44 C55E F096 A89C 8D08
    
    gpg> trust
    pub  dsa3072 created: 2022-02-11  expires: never       usage: SC
                         trust: unknown       validity: unknown
    [ unknown] (1). TumuxOS Master Key
    
    Please decide how far you trust this user to correctly verify other users' keys
    (by looking at passports, checking fingerprints from different sources, etc.)
    
       1 = I don't know or won't say
       2 = I do NOT trust
       3 = I trust marginally
       4 = I trust fully
       5 = I trust ultimately
       m = back to the main menu
    
    Your decision? 5
    Do you really want to set this key to ultimate trust? (y/N) y
    
    pub  dsa3072  created: 2022-02-11  expires: never       usage: SC
                         trust: ultimate      validity: unknown
    [ unknown] (1). TumuxOS Master Key
    Please note that the shown key validity is not necessarily correct
    unless you restart the program.
    
    gpg> q
```

Now, when you import any of the release signing keys and many keys from team member, they will already be trusted in virtue of being signed by the TMSK.

As a final sanity check, make sure the TMSK is in your keyring with the correct trust level.
```bash
    $ gpg2 -k "TumuxOS Master Key"
    pub   dsa3072 2022-02-11 [SC]
          E66C6215074B6FDFEBB26E44C55EF096A89C8D08
    uid          [  ultime ] TumuxOS Master Key
```

If you don’t see the TMSK here with a trust level of “ultimate,” go back and follow the instructions in this section carefully and consult the [troubleshooting FAQ](#troubleshooting-faq) below.

How to authenticate other keys
------------------------------

Before we proceed, you must first complete the following prerequisite steps:

1.  [Install OpenPGP software.](#openpgp-software)
2.  [Import and authenticate the TMSK.](#how-to-import-and-authenticate-the-tumuxos-master-signing-key)

You can obtain the dev's key or any release key using the same methode as for the TMSK. For exemple, you can find all of the project's key in the [security repository of TumuxOS](https://gitlab.com/tumuxos/Security) (in the "keys" directory).


Now that you have the key you want to validate, you simply need to verify that it is signed by the TMSK:
```bash
    $ gpg2 --check-signatures "TumuxOS Backup Master Key"
	pub   dsa3072 2022-02-11 [SC]
		  BCE5C7F956B244B3940F49E968F902051F4B9D3A
	uid          [  ultime ] TumuxOS Backup Master Key
	sig!3        68F902051F4B9D3A 2022-02-11  TumuxOS Backup Master Key
	sig!         DC773C5B6D417714 2022-02-11  Tutul (TUTUL-SUPERTUX) <raphael.slagmolen@gmail.com>
	sig!         C55EF096A89C8D08 2022-02-11  TumuxOS Master Key

	gpg: 3 good signatures
```

This is just an example, so the output you receive may not look exactly the same. What matters is the line with a `sig!` prefix showing that the TMSK has signed this key. A `sig-` prefix would indicate a bad signature, and `sig%` would mean that gpg encountered an error while verifying the signature. It is not necessary to independently verify the authenticity of those keys, since you already verified the authenticity of the TMSK.

As a final sanity check, make sure the key is in your keyring with the correct trust level:
```bash
    $ gpg2 -k "TumuxOS Backup Master Key"
	pub   dsa3072 2022-02-11 [SC]
		  BCE5C7F956B244B3940F49E968F902051F4B9D3A
	uid          [  ultime ] TumuxOS Backup Master Key
```

If you don’t see the correct key here with a trust level of “full” or higher, go back and follow the instructions in this section carefully, and consult the [troubleshooting FAQ](#troubleshooting-faq) below.

How to verify the cryptographic hash values of a release
--------------------------------------------------------

There are two ways to verify a release: cryptographic hash values and detached PGP signatures. Both methods are equally secure. Using just one method is sufficient to verify the file. Using both methods is not necessary, but you can do so if you like. One method might be more convenient than another in certain circumstances, so we provide both. This section covers cryptographic hash values. For the other method, see [how to verify detached PGP signatures](#how-to-verify-detached-pgp-signatures).

Before we proceed, you must first complete the following prerequisite steps:

1.  [Install OpenPGP software.](#openpgp-software)
2.  [Import and authenticate the TMSK.](#how-to-import-and-authenticate-the-tumuxos-master-signing-key)
3.  [How to authenticate other keys](#how-toauthenticate-other-keys) (the one that signed those file)

Each release is accompanied by a set of **cryptographic hash values** contained in a plain text file ending in `.DIGESTS`. This file contains the output of running several different cryptographic hash functions on the release archive or file (a process known as “hashing”) to obtain alphanumeric outputs known as “hash values” or “digests”.

In addition to the `.DIGESTS` files alongside each release, and you can always find all the digest files for every release in the [security repository of TumuxOS](https://gitlab.com/tumuxos/Security) (in the "release" directory).

If the filename is `TumuxOS-x86_64.zip`, then the name of the digest file for it is `TumuxOS-x86_64.zip.DIGESTS`. Since the digest file is a plain text file, you can open it with any text editor. Inside, you should find text that looks similar to this:
```
    -----BEGIN PGP SIGNED MESSAGE-----
    Hash: SHA256
    
    8657f173c13c951138b8b9867db58b82 *TumuxOS-x86_64.zip
    c4cba6cacc3025165b0f996c8431fc9508160d7f *TumuxOS-x86_64.zip
    4f1b4e7eff796b998045a513dcdd45c1c6e61adccb9eb0170c80f678ace9f381 *TumuxOS-x86_64.zip
	fe344027eced13cd02fe20658d4a7f09375e7a4a4499d7bff484d7a6214ba00d4e40c63723171c62bdcc86c3acfe9c2b221baee497de1eb2e76bdb48559906f6 *TumuxOS-x86_64.zip
    -----BEGIN PGP SIGNATURE-----
    Version: GnuPG v2
    
    6EkFc2lkQHbgZxjXqyrEMbgeSXtMltZ7cCqw1
    3N/6YZw1gSuvBlTquP27/jlZ26zhvlDEX/eaA/ANa/6b
    Dpsh/sqZEpz1SWoUxdm0gS+anc8nSDoCQSMBxnafuBbmwTChdHI/P7NvNirCULma
    9nw+EYCsCiNZ9+WCeroR8XDFSiDjvfkve0R8nwfma1XDqu1bN2ed4n/zNoGgQ8w0
    t5LEVDKCVJ+65pI7RzOSMbWaw+uWfGehbgumD7a6rfEOqOTONoZOjJJTnM0+NFJF
    Qz5yBg+0FQYc7FmfX+tY801AwSyevj3LKGqZN1GVcU9hhoHH7f2BcbdNk9I5WHHq
    doKMnZtcdyadQGwMNB68Wu9+0CWsXvk6E00QfW69M4d6w0gbyoJyUL1uzxgixb5O
    qodxrqeitXQSZZvU4kom5zlSjqZs4dGK+Ueplpkr8voT8TSWer0Nbh/VMfrNSt1z
    0/j+e/KMjor7XxehR+XhNWa2YLjA5l5H9rP+Ct/LAfVFp4uhsAnYf0rUskhCStxf
    Zmtqz4FOw/iSz0Os+IVcnRcyTYWh3e9XaW56b9J/ou0wlwmJ7oJuEikOHBDjrUph
    2a8AM+QzNmnc0tDBWTtT2frXcotqL+Evp/kQr5G5pJM/mTR5EQm7+LKSl7yCPoCj
    g8JqGYYptgkxjQdX3YAy9VDsCJ/iQIcBAEBCAAGBQJX4XO/AAoJEMsRyh0D+lCCL9sP
    =e9oD
    -----END PGP SIGNATURE-----
```

The hash functions used, in order from top to bottom, are MD5, SHA-1, SHA-256, and SHA-512. One way to verify that the file you downloaded matches any of these hash values is by using the respective `*sum` command:
```bash
    $ md5sum -c TumuxOS-x86_64.zip.DIGESTS
     TumuxOS-x86_64.zip: OK
    md5sum: WARNING: 23 lines are improperly formatted
    $ sha1sum -c TumuxOS-x86_64.zip.DIGESTS
    TumuxOS-x86_64.zip: OK
    sha1sum: WARNING: 23 lines are improperly formatted
    $ sha256sum -c TumuxOS-x86_64.zip.DIGESTS
    TumuxOS-x86_64.zip: OK
    sha256sum: WARNING: 23 lines are improperly formatted
    $ sha512sum -c TumuxOS-x86_64.zip.DIGESTS
    TumuxOS-x86_64.zip: OK
    sha512sum: WARNING: 23 lines are improperly formatted
```

The `OK` response tells us that the hash value for that particular hash function matches. The program also warns us that there are 23 improperly formatted lines, but this is expected. This is because each file contains lines for several different hash values (as mentioned above), but each `*sum` program verifies only the line for its own hash function. In addition, there are lines for the PGP signature that the `*sum` programs do not know how to read. Therefore, it is safe to ignore these warning lines.

However, it is possible that an attacker replaced `TumuxOS-x86_64.zip` with a malicious file, computed the hash values for that malicious file, and replaced the values in `TumuxOS-x86_64.zip.DIGESTS` with his own set of values. Therefore, we should also verify the authenticity of the listed hash values. Since `TumuxOS-x86_64.zip.DIGESTS` is a clearsigned PGP file, we can use GPG to verify the signature in the digest file:
```bash
    $ gpg2 -v --verify TumuxOS-x86_64.zip.DIGESTS
    gpg: armor header: Hash: SHA256
    gpg: armor header: Version: GnuPG v2
    gpg: original file name=''
    gpg: Signature made Tue 20 Sep 2022 10:37:03 AM PDT using DSA key ID C55EF096A89C8D08
    gpg: using PGP trust model
    gpg: Good signature from "TumuxOS Master Key"
    gpg: textmode signature, digest algorithm SHA256
```

This is just an example, so the output you receive will not look exactly the same. What matters is the line that says `Good signature from <TumuxOS valid key>`. This confirms that the signature on the digest file is good.

If you don’t see a good signature here, go back and follow the instructions in this section carefully, and consult the [troubleshooting FAQ](#troubleshooting-faq) below.

How to verify detached PGP signatures
-------------------------------------

There are two ways to verify: cryptographic hash values and detached PGP signatures. Both methods are equally secure. Using just one method is sufficient. Using both methods is not necessary, but you can do so if you like. One method might be more convenient than another in certain circumstances, so we provide both. This section covers detached PGP signatures. For the other method, see [How to verify the cryptographic hash values of a release](#how-to-verify-the-cryptographic-hash-values-of-a-release).

Before we proceed, you must first complete the following prerequisite steps:

1.  [Install OpenPGP software.](#openpgp-software)
2.  [Import and authenticate the TMSK.](#how-to-import-and-authenticate-the-tumuxos-master-signing-key)
3.  [How to authenticate other keys](#how-toauthenticate-other-keys) (the one that signed those file)

Every release is provided with a **detached PGP signature** file, which you can find alongside the release with the `.asc` extention. You can always find all the detached signature files for every release in the [security repository of TumuxOS](https://gitlab.com/tumuxos/Security) (in the "release" directory).

Download both the release file/archive and its signature file. Put both of them in the same directory, then navigate to that directory. Now, you can execute this GPG command in the directory that contains both files to validate the downloaded file:
```bash
    $ gpg2 -v --verify TumuxOS-x86_64.zip.asc TumuxOS-x86_64.zip
    gpg: armor header: Version: GnuPG v1
    gpg: Signature made Tue 08 Mar 2022 07:40:56 PM PST using DSA key ID C55EF096A89C8D08
    gpg: using PGP trust model
    gpg: Good signature from "TumuxOS Master Key"
    gpg: binary signature, digest algorithm SHA256
```

This is just an example, so the output you receive will not look exactly the same. What matters is the line that says `Good signature from <TumuxOS valid key>`. This confirms that the signature on the digest file is good.

If you don’t see a good signature here, go back and follow the instructions in this section carefully, and consult the [troubleshooting FAQ](#troubleshooting-faq) below.

How to verify signatures on Git repository tags and commits
-----------------------------------------------------------

Before we proceed, you must first complete the following prerequisite steps:

1.  [Install OpenPGP software.](#openpgp-software)
2.  [Import and authenticate the TMSK.](#how-to-import-and-authenticate-the-tumuxos-master-signing-key)
3.  [How to authenticate other keys](#how-toauthenticate-other-keys) (those from the developer team)

Whenever you use one of the [TumuxOS repositories](https://gitlab.com/tumuxos/), you should use Git to verify the PGP signature in a tag on the latest commit or on the latest commit itself. (One or both may be present, but only one is required.) If there is no trusted signed tag or commit on top, any commits after the latest trusted signed tag or commit should **not** be trusted. If you come across a repo with any unsigned commits, you should not add any of your own signed tags or commits on top of them unless you personally vouch for the trustworthiness of the unsigned commits. Instead, ask the person who pushed the unsigned commits to sign them.

You should always perform this verification on a trusted local machine with properly authenticated keys rather than relying on a third party, such as GitLab. While the GitLab interface may claim that a commit has a verified signature, this is only trustworthy if GitLab has performed the signature check correctly, the account identity is authentic, an admin has not replaced the user’s key, GitLab’s servers have not been compromised, and so on. Since there’s no way for you to be certain that all such conditions hold, you’re much better off verifying signatures yourself. (Also see: [distrusting the infrastructure](/faq/#what-does-it-mean-to-distrust-the-infrastructure).)

### How to verify a signature on a Git tag  

    $ git tag -v <tag name>
    

or

    $ git verify-tag <tag name>
    

### How to verify a signature on a Git commit 

    $ git log --show-signature <commit ID>
    

or

    $ git verify-commit <commit ID>
    

Troubleshooting FAQ
-------------------

### Why am I getting “Can’t check signature: public key not found”? 

You don’t have the correct [signing keys](#how-toauthenticate-other-keys)

### Why am I getting “BAD signature from ‘<TumuxOS-like key>’”? 

The problem could be one or more of the following:

*   You’re trying to verify the wrong file(s). Reread this page carefully.
*   You’re using the wrong GPG command. Follow the provided examples carefully, or try using `gpg` instead of `gpg2` (or vice versa).
*   The file or [detached PGP signature file](#how-to-verify-detached-pgp-signatures) is bad (e.g., incomplete or corrupt download). Try downloading the signature file again from a different source, then try verifying again.
* The file you try to verify has been compromised.

### Why am I getting “bash: gpg2: command not found”? 

You don’t have `gpg2` installed. Please install it using the method appropriate for your environment (e.g., via your package manager), or try using `gpg` instead.

### Why am I getting “No such file or directory”? 

Your working directory does not contain the required files. Go back and follow the instructions more carefully, making sure that you put all required files in the same directory _and_ navigate to that directory.

### Why am I getting “WARNING: This key is not certified with a trusted signature! There is no indication that the signature belongs to the owner.”? 

There are several possibilities:

*   You don’t have the [TumuxOS Master Signing Key](#how-to-import-and-authenticate-the-tumuxos-master-signing-key).
*   You have not [set the TumuxOS Master Signing Key’s trust level correctly.](#how-to-import-and-authenticate-the-tumuxos-master-signing-key)
* The key isn't a valid key for the project or has been revoked and should be trusted

### Why am I getting “X signature not checked due to a missing key”? 

You don’t have the keys that created those signatures in your keyring. You are missing an other key from the chain of trust, you may need to [import an other key](#how-toauthenticate-other-keys).

### Why am I seeing additional signatures on a key with “\[User ID not found\]” or from a revoked key? 

This is just a fundamental part of how OpenPGP works. Anyone can sign anyone else’s public key and upload the signed public key to keyservers. Everyone is also free to revoke their own keys at any time (assuming they possess or can create a revocation certificate). This has no impact on verifying anything.

### Why am I getting “verify signatures failed: unexpected data”? 

You’re not verifying against the correct [detached PGP signature file](#how-to-verify-detached-pgp-signatures).

### Why am I getting “not a detached signature”? 

You’re not verifying against the correct [detached PGP signature file](#how-to-verify-detached-pgp-signatures).

### Why am I getting “CRC error; \[…\] no signature found \[…\]”? 

You’re not verifying against the correct [detached PGP signature file](#how-to-verify-detached-pgp-signatures), or the signature file has been modified. Try downloading it again or from a different source.

### Do I have to verify both the [detached PGP signature file](#how-to-verify-detached-pgp-signatures) and the [cryptographic hash values](#how-to-verify-the-cryptographic-hash-values)? 

No, either method is sufficient by itself, but you can do both if you like.

### Why am I getting “no properly formatted X checksum lines found”? 

You’re not checking the correct [cryptographic hash values](#how-to-verify-the-cryptographic-hash-values).

### Why am I getting “WARNING: X lines are improperly formatted”? 

Read [how to verify the cryptographic hash values](#how-to-verify-the-cryptographic-hash-values) again.

### I have another problem that isn’t mentioned here. 

Carefully reread this page to be certain that you didn’t skip any steps. If your question is about GPG, please see the [GnuPG documentation](https://www.gnupg.org/documentation/). Still have question? Please ask us or a trusted friends that know the project.
